from typing import List, Tuple

from numpy import inf
import pulp

# Base Scheduling class, only add a method here if all classes will need it
class Scheduling():
    name: str = "Base"
    images: List[float]
    solution: List[float]
    unavailable: List[Tuple[float, float]]

    def __init__(self, test_case: Tuple[List[float], List[Tuple[float, float]]]) -> None:
        '''Initializes the Scheduling class

        Args:
            test_case (Tuple[List[float], List[Tuple[float, float]]]): The test case to initialize on
        '''
        self.images, self.unavailable, self.solution = test_case

# Example of an implementation class, For now they should only contain the offline and online method
class DumbScheduling(Scheduling):
    name = "Dumb Scheduling"

    def offline(self):
        result: List[Tuple[float, int]] = [] # list of (starttime, image idx)
        curtime = 0
        i = 0
        j = 0
        
        while i <= len(self.images) and j <= len(self.unavailable):
            if i == len(self.images):
                return (result, curtime)
            elif j == len(self.unavailable):
                for k in range(len(self.images) - i):
                    result.append((curtime, i + k))
                    curtime += self.images[i + k]
                return (result, curtime)
            elif self.images[i] + curtime <= self.unavailable[j][0]:
                result.append((curtime, i))
                curtime += self.images[i]
                i += 1
            else:
                curtime = self.unavailable[j][0] + self.unavailable[j][1]
                j += 1
        
        raise RuntimeError('Unexpected error while running DumbScheduling')

class GreedyScheduling(Scheduling):
    '''Scheduling that prefers planning large objects first'''
    name = "Greedy Scheduling"

    def offline(self) -> Tuple[List[float], float]:
        '''Provides a solution for the offline variant of this problem
        
        Gap is defined as a period of time where a image can be send e.g. no interuption.

        Returns:
            Tuple[List[float], float]: The start times of each image to send and the total time it takes
        '''
        unavailable = self.unavailable
        unavailable.append((0.0, 0.0))                                          # Add start initial gap
        interruptions = sorted(unavailable, key=lambda t: t[0])                 # Sort the interruptions first
        gaps: List[Tuple[float, float]] = []                                    # Stores gaps between intervals
        
        for i in range(len(interruptions) - 1):                                 # Iterate through every interruption except the last
            last_end = interruptions[i][0] + interruptions[i][1]                # Define starting point of the gap
            gaps.append((last_end, interruptions[i + 1][0] - last_end))         # Add gap to list from starting point until next interuption

        gaps.append((interruptions[-1][0] + interruptions[-1][1], inf))         # Create the last 'gap' with infinite length

        images = sorted(enumerate(self.images), key=lambda t: -t[1])            # Sort the images by length (large to small)
        result = [0.0] * len(self.images)                                       # Create a list for results
        total = 0.0                                                             # Total time it takes to clear

        for gi, (start, length) in enumerate(gaps):
            delete_images: List[int] = []

            for ii, (index, size) in enumerate(images):
                if size <= length:
                    result[index] = start                                       # Assign the result of the image as the start of the gap under inspection
                    delete_images.append(ii)                                    # Mark that the image should be removed from the to-do list
                    total = start + size                                        # Update the total time
                    gaps[gi] = (start + size, length - size)                    # Resize the gap appropriately
                    (start, length) = gaps[gi]                                  # Update local variables
                    
            delete_images.reverse()

            for di in delete_images:
                del images[di]

        return (result, total)

class ILPScheduling(Scheduling):
    '''Scheduling that uses an ILP'''
    name = "ILP Scheduling"

    def offline(self) -> Tuple[List[float], float]:
        '''Provides a solution for the offline variant of this problem
        
        Job is defined as sending an image
        Timeslot is defined as a period of time where a Job can be send e.g. no interuption.

        Returns:
            Tuple[List[float], float]: The start times of each image to send and the total time it takes
        '''
        timeslots = []
        total_image_size = sum(self.images)                                                             # Calculate the size of all jobs to create a timeslot at the end which will fit all images
        interruption_end = 0 if len(self.unavailable) == 0 else sum(self.unavailable[-1])               # Calculate the end time of the last interruption, if there are no interruptions the end is at 0
        interruptions = [(0,0)] + self.unavailable + [(interruption_end + total_image_size, 0)]         # Add an start interuption and a end interuption so that the loops dont go out of bounds


        for i in range(len(interruptions)-1):
            timeslots.append((interruptions[i][0] + interruptions[i][1], interruptions[i+1][0]))    # Declare a timeslot between all ends of interuptions and start of the next interuption
        
        problem = pulp.LpProblem("scheduling", pulp.LpMinimize)         # Create an LP problem, with as goal minimization
        solver = pulp.PULP_CBC_CMD(keepFiles=False, msg=False)          # Declare a solver which doesn't print or keeps the files after completion.

        # Vars
        b = [pulp.LpVariable("b_" + str(i), lowBound=0, upBound=1, cat="Binary") for i in range(len(timeslots)+1)]  # Binary var to show if a timeslot is used
        z = [pulp.LpVariable("z_" + str(i), lowBound=0, upBound=1, cat="Binary") for i in range(len(timeslots))]    # Binary var indicating the last timeslot used
        x = [                                                                                                       # 2D-array of Binary vars indicating if job j is done in timeslot i
                [
                    pulp.LpVariable(
                        "x_" + str(i) + "," + str(j),
                        lowBound=0,
                        upBound=1,
                        cat="Binary"
                    ) for j in range(len(self.images))
                ] for i in range(len(timeslots))
            ]
        y = pulp.LpVariable("y", lowBound=0)                                                                        # Remaining time of the last used timeslot

        # Cons
        a = [timeslot[1] - timeslot[0] for timeslot in timeslots]                                                   # Total usable time in a timeslot
        l = self.images                                                                                             # Size of a job              
        t = 4 * sum(a)                                                                                              # Big number to bound y correctly

        problem += pulp.lpSum(b[i]*a[i] for i in range(len(timeslots))) - y                                         # Objective function

        problem += pulp.lpSum(z[i] for i in range(len(timeslots))) == 1                                             # There can only be 1 timeslot
        for i in range(len(timeslots)):
            problem += pulp.lpSum(x[i][j] *l[j] for j in range(len(self.images))) <= a[i]                           # Enforce max size of timeslot
            problem += y <= t * (1-z[i]) + a[i] - pulp.lpSum(x[i][j] * l[j] for j in range(len(self.images)))       # Bound y to the last used timeslot
            problem += z[i] <= b[i]                                                                                 # If a timeslot isn't used it cannot be the last used
            problem += z[i] <= (1-b[i+1])                                                                           # If the next timeslot isn't used the current timeslot could be the last used timeslot
            problem += b[i+1] <= b[i]                                                                               # If the next timeslot is used the current timeslot must also be used
            for j in range(len(self.images)):
                problem += b[i] >= x[i][j]                                                                          # If there is a job scheduled in the current timeslot the current timeslot must be used.
        for j in range(len(self.images)):
            problem += pulp.lpSum(x[i][j] for i in range(len(timeslots))) == 1                                      # Each job must be scheduled once

        problem.solve(solver)

        image_in_timeslot = {}                                              # Dictionary of form Dict[int, List[int]] where the key is the timeslot and the value a list of jobs
        for v in problem.variables():
            if v.name[0] == 'x' and v.varValue == 1:                        # If the variable is the assingment variable and value is 1, then the variable is an assignment for job j.
                data = v.name[2:].split(',')                                # Parse the variable name to get the indices for image and timeslot
                if int(data[0]) in image_in_timeslot.keys():                # If an image was already added to a timeslot we need to add this image to the list
                    image_in_timeslot[int(data[0])].append(int(data[1]))
                else:                                                       # If no image was added to a timeslot we need to declaire a key-value pair
                    image_in_timeslot[int(data[0])] = [int(data[1])]    

        schedule = [None] * len(self.images)                                # Declare the schedule as a list of the same size as the images
        for i in range(len(timeslots)):
            if i not in image_in_timeslot:                                  # If the image doesn't exist in our dictionary it means no images are assigned to this timeslot
                continue
            cur_interruption = interruptions[i]                             # Get the interrruption before this timeslot
            images_in_cur_timeslot = image_in_timeslot[i]                   # Get all images that are assigned to this timeslot
            cur_time = cur_interruption[0] + cur_interruption[1]            # Get the start time of this timeslot
            for j in images_in_cur_timeslot:
                schedule[j] = cur_time                                      # Assign image at the first moment of free time
                cur_time += self.images[j]                                  # Add the size of the image to show when the next free time begins

        return (schedule, cur_time)
