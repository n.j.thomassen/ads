## Install Dependencies

To install the required libaries you can use one of these depending on your terminal and setup.

```
> pip install -r requirements.txt
> python -m pip install -r requirements.txt
> pip3 install -r requirements.txt
```


## How to run

To run the code simply run `run.py` in your terminal.

```
> python3 run.py
```

## Test cases

In order to test the code with a testcase enter the testcase in the `test_cases` folder.

The program allows you to run multiple different testcases as long as it follows one of the following rules.

1. The testcase is in a different file in the `test_cases` folder
2. Multiple testcases in a file are seperated by a blank line

## Programmer notice

If you want to add an extra implementation please note that you will need to inherit the scheduling base class in order to get the initialize and potentially other shared methods

## Example of  input/output

For the case where there are 5 images (of sizes `0.618, 2.5, 10, 1, and 3.14`) and 2 unavailable time intervals `([4, 4.25) and [5, 16))`, the input will be:

```
5
0.618
2.5
10
6
3.14
2
4, 0.25
5, 11
```

Here the first line is the number (5) of images. The following 5 lines consist 5 real numbers, which are the sizes of the images. After that, the 7-th line is the number (2) of unavailable time intervals. Each of the last two lines consists two real numbers, where the first one is the start time of the first/second unavailable time interval,and the second one is the length of the first/second unavailable time interval. `(start_time, length)`.

For this instance, the output can be:

```
41.618
35
29.6
18
35.618
0.86
```
In the output, the first lines is the time when all images are transmitted. The next n lines are the times when the 1-st to the n-th images start transmitting, respectively.
