import copy
from doctest import UnexpectedException
import os
from typing import List, Tuple, Union

from implementation import DumbScheduling, Scheduling, GreedyScheduling, ILPScheduling

# Add your extra Scheduling implementations here to add it to the tester, don't forget to also import it.
implementations: List[Scheduling] = [
    ILPScheduling,
    #DumbScheduling,
]

def read_files() -> List[Tuple[List[float], List[Tuple[float, float]]]]:
    '''Reads files from the './test_cases/' folder

    Returns:
        List[Tuple[List[float], List[Tuple[float, float]]]]: Returns a list of tuples each containing the images to send and the intervals of the outages (start time and length)
    '''
    result = []
    test_cases = os.listdir('./test_cases')

    for test_case in test_cases:
        with open('./test_cases/' + test_case, 'r') as f:
            lines = [line[:-1] for line in f.readlines() if line[:-1] != '']     # Strip the lines of their '\n' and remove empty lines
        line_index = 0
        while line_index < len(lines):
            case, line_index = parse_case(lines, line_index)
            result.append((test_case, case))

    return result

def parse_case(lines: List[str], start_index: int = 0) -> Tuple[Tuple[List[float], List[Tuple[float, float]], Union[List[float], None]], int]:
    '''Parses a single test case from a test case file

    Args:
        lines (List[str]): The lines in the test case file
        start_index (int, optional): The index to start reading at. Defaults to 0.

    Returns:
        Tuple[Tuple[List[float], List[Tuple[float, float]], Union[List[float], None]]: A tuple containing the parsed test case and the index of the next line
    '''
    image_count = int(lines[start_index])
    images = []

    for i in range(start_index + 1, start_index + 1 + image_count):
        images.append(float(lines[i]))

    break_count = int(lines[start_index + 1 + image_count])
    breaks = []

    for i in range(start_index + 2 + image_count, start_index + 2 + image_count + break_count):
        start, length = lines[i].split(',')
        breaks.append((float(start), float(length)))

    # Check for results
    next_line = start_index + 2 + image_count + break_count

    try:
        int(lines[next_line])
    except ValueError:
        try:
            float(lines[next_line])
        except ValueError:
            raise ValueError('Unexpected token on line %d' % next_line)
    except IndexError:
        return ((images, breaks, None), start_index + 2 + image_count + break_count)
    else:
        return ((images, breaks, None), start_index + 2 + image_count + break_count)

    # Parse results
    solution = []

    for i in range(next_line, next_line + image_count):
        solution.append(float(lines[i]))

    return ((images, breaks, solution), next_line + image_count)

def run() -> None:
    '''Runs the main program'''
    test_cases = read_files()
    for idx, (test_case, case) in enumerate(test_cases):
        for implementation in implementations:
            current_case = copy.deepcopy(case)
            implemented = implementation(current_case)

            try:
                result, total_time = implemented.offline()
            except IndexError:
                print("An error occured for case %s idx %s with implementation \"%s\", see case below" % (test_case, idx, implemented.name))
                print(current_case)
            else:
                print("Case file %s with idx %s with implementation %s had a total time of \"%s\" see schedule below" % (test_case, idx, implemented.name, total_time))
                print(result)
                print("----")
    # Todo: If called for we'd probably want to add some runtime timer here, We'd also might want to add some way for the case idx to be searchable if needed.

if __name__=='__main__':
    run()
